﻿using FlightBookingSystem.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightBookingSystem.Service
{
   public interface IFlightBookingSystemService
    {
        Task<int> AddPassenger(Passenger passenger);

        Task<List<Flight>> GetFlights();

        Task<List<Passenger>> GetPassenger( String flight);
    }
}
