﻿using FlightBookingSystem.Models;
using FlightBookingSystem.Repository;
using FlightManagementSystem.CustomExceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightBookingSystem.Service
{
    public class FlightBookingSystemService:IFlightBookingSystemService
    {
        readonly IFlightBookingSystemRepository IflightBookingSystemRepository;
        private readonly ILogger logger;

        public FlightBookingSystemService(IFlightBookingSystemRepository data, ILogger<FlightBookingSystemRepository> data2)
        {
            IflightBookingSystemRepository = data;
            logger = data2;

        }




        public async Task<int> AddPassenger(Passenger passenger)
        {
            if (passenger.FlightId == 0)
            {

                logger.LogWarning(1,"Flight Id Should not zero", "There was a exception in AddPassengersDetailsWithgFlightName {Time}", DateTime.Now);


                throw new MissingFlightDetailsException("Passenger should contain flight details");
            }

            try
            {

                return await IflightBookingSystemRepository.AddPassenger(passenger);
            }
            catch (Exception ex)
            {

                Exception exception = (ex.InnerException != null) ? ex.InnerException : ex;

                logger.LogError(exception, "Exception in AddPassenger");

                throw new Exception(ex.Message);
            }
        }




        public async Task<List<Flight>> GetFlights()
        {
            try
            {
                logger.LogTrace("Trace");
                logger.LogDebug("Debug");
                
                logger.LogInformation("Info");
                logger.LogWarning("Warn");
                logger.LogError("Error");
                logger.LogCritical("Critical");

                return await IflightBookingSystemRepository.GetFlights();
            }
            catch (Exception ex)
            {
                Exception exception = (ex.InnerException != null) ? ex.InnerException : ex;

                logger.LogError(2,exception, "Exception in GetFlights");

                throw new Exception(ex.Message);
            }
        }


        public async Task<List<Passenger>> GetPassenger([FromBody]string flight)
        {

            try
            {
                logger.LogInformation(3,"Calling GetPassenger methode");

                return await IflightBookingSystemRepository.GetPassenger(flight);
            }
            catch (Exception ex)
            {

               

                Exception exception = (ex.InnerException != null) ? ex.InnerException : ex;


                logger.LogTrace(4,"GetPassenger funcion", DateTime.Now);

                logger.LogError(4, exception, "There was a exception in GetPassengersDetailsFromFlightName {Time}", DateTime.Now);

                 //logger.LogError(ex.Message);
                //log4net.Error(ex.Message);

                throw new Exception(ex.Message);
            }
        }
    }
}




//Log Providers
/*Console
  Debug
  EventSource
  EventLog: Windows only*/

//Logging levels
/*_logger.LogTrace("");
_logger.LogDebug("");
_logger.LogTrace("");
_logger.LogInformation("");
_logger.LogWarning("");
_logger.LogError("");
_logger.LogCritical("");*/