﻿using FlightBookingSystem.Models;
using FlightBookingSystem.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightBookingSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightBookingSystemController : ControllerBase
    {
        readonly IFlightBookingSystemService IflightBookingSystemService;

        public FlightBookingSystemController(IFlightBookingSystemService data)
        {
            IflightBookingSystemService = data;
        }

        [HttpPost]
        [Route("AddPassenger")]
        public async Task<IActionResult> AddPassenger([FromBody] Passenger passenger)
        {
            try
            {

                await IflightBookingSystemService.AddPassenger(passenger);




              

                return Created("AddPassenger", new { success = "passengers added" })
            







            }
            catch (Exception ex)
            {

                return BadRequest(new { falied = ex.Message });
            }

            
        }




        [HttpGet]
        [Route("GetFlights")]
        public async Task<IActionResult> GetFlights()
        {
            try
            {
                return Ok(await IflightBookingSystemService.GetFlights());
            }
            catch (Exception)
            {
                return BadRequest("something Wrong...");
            }
        }





        [HttpGet]
        [Route("GetPassenger")]
        public async Task<IActionResult> GetPassenger( string flight)
        {
            try
            {
                return Ok(await IflightBookingSystemService.GetPassenger(flight));
            }
            catch (Exception)
            {
                return BadRequest("something Wrong...");
            }
        }



    }
}
