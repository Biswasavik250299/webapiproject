﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FlightBookingSystem.Models
{
    [Table("Passenger")]
    public partial class Passenger
    {
        [Key]
        public int PassengerId { get; set; }
        [Required]
        [StringLength(60)]
        public string PassengerName { get; set; }
        public int? FlightId { get; set; }
        public int? Age { get; set; }

        [ForeignKey(nameof(FlightId))]
        [InverseProperty("Passengers")]
        public virtual Flight Flight { get; set; }
    }
}
