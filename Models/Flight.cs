﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FlightBookingSystem.Models
{
    [Table("Flight")]
    public partial class Flight
    {
        public Flight()
        {
            Passengers = new HashSet<Passenger>();
        }

        [Key]
        public int FlightId { get; set; }
        [Required]
        [StringLength(60)]
        public string Name { get; set; }

        [InverseProperty(nameof(Passenger.Flight))]
        public virtual ICollection<Passenger> Passengers { get; set; }
    }
}
