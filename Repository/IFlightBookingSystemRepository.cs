﻿using FlightBookingSystem.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightBookingSystem.Repository
{
   public interface IFlightBookingSystemRepository
    {
        Task<int> AddPassenger(Passenger passenger);

        Task<List<Flight>> GetFlights();
        Task<List<Passenger>> GetPassenger(string flight);
    }
}

