﻿using FlightBookingSystem.Models;
using FlightManagementSystem.CustomExceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightBookingSystem.Repository
{
    public class FlightBookingSystemRepository:IFlightBookingSystemRepository
    {
        readonly FlightBookingSystemContext flightBookingSystemContext;

        public FlightBookingSystemRepository(FlightBookingSystemContext data)
        {
            flightBookingSystemContext = data;
        }



        public async Task<int> AddPassenger(Passenger passenger)
        {
            try
            {
                
                    await flightBookingSystemContext.Passengers.AddAsync(passenger);

                    return await flightBookingSystemContext.SaveChangesAsync();

            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("Can't connect to database", ex);
            }

        }


        public async Task<List<Flight>> GetFlights()
        {
            try
            {

                var details = await flightBookingSystemContext.Flights.ToListAsync();

                return details;
            }

            catch (SqlException ex)
            {

                throw new CustomSqlException("Can't connect to database", ex);
            }
        }




        public async Task<List<Passenger>> GetPassenger(string flight)
        {
            try
            {

                var flightId = flightBookingSystemContext.Flights.Where(f => f.Name == flight).Select(p => p.FlightId).SingleOrDefault();

                if (flightId == 0)
                {

                    throw new FlightNameNotFoundException("Invalid flight name!!");
                }

                var passengerDetails = await flightBookingSystemContext.Passengers.Where(p => p.FlightId == flightId).ToListAsync();

                if (passengerDetails.Count == 0)
                {

                    throw new PassengersDetailsNotFoundException("No details found for passenger in flight!!!");
                }

                return passengerDetails;

            }
            catch (SqlException ex)
            {

                throw new CustomSqlException("Can't connect to database", ex);
            }

        }




    }
}
